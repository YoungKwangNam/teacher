import tkinter as tk
import os, socket
import threading
global HOST, PORT
HOST = '192.168.137.1'  # server ip
PORT = 9999  # server port

class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.master.geometry('500x300+100+100')  # 윈도우창 크기 1600*900, 위치:100,100
        self.master.resizable(True, True)
        self.pack()
        self.create_widgets()

    def create_widgets(self):
        global HOST, PORT
        self.title = tk.Label(self, width=30, font=60, text='image transfer app server')
        self.title.pack()
        self.init_str = '<connect state> \nserver ip:'+HOST+'/port:'+str(PORT)+'\n'
        self.msgs = [self.init_str,'','','','','']
        self.msg_cnt=1

        self.state = tk.Label(self, width=30, font=60, text=self.init_str)
        self.state.pack()

        self.img = tk.PhotoImage(file="")
        self.img_viewer = tk.Label(self.master, image=self.img)
        self.img_viewer.pack()

        self.slide = tk.Button(self, width=10, font=60, text='next')
        self.slide.pack()


root = tk.Tk()
app = Application(master=root)

cnt = 0
imgs = None


def slide_event():
    global imgs
    global cnt

    imgs = os.listdir('imgs')
    size = len(imgs)

    app.img = tk.PhotoImage(file='imgs/'+imgs[cnt % size])
    app.img_viewer["image"] = app.img
    cnt += 1


def stop_event():
    global slide_flag
    slide_flag = False



def upload(soc):
    data = soc.recv(1024) #file name / size
    data2 = data.decode()
    k = data2.split('/')
    f_name = k[0] #file name
    f_size = int(k[1]) #file size
    f = open('imgs/' + f_name, 'wb')
    data = soc.recv(f_size)
    print('body:', data)
    f.write(data)
    f.close()
    app.img=tk.PhotoImage(file='imgs/'+f_name)
    app.img_viewer["image"]=app.img
    #soc.close()

def server():
    global HOST, PORT

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind((HOST, PORT))
    server_socket.listen()
    while True:
        client_socket, addr = server_socket.accept()
        app.msgs[app.msg_cnt]='client:'+str(addr)+' connect =>picture upload\n'
        app.msg_cnt+=1
        if app.msg_cnt==len(app.msgs):
            app.msg_cnt=1
        s=''
        for i in app.msgs:
            s+=i

        app.state.configure(text=s)
        upload(client_socket)


app.slide['command'] = slide_event
t=threading.Thread(target=server,args=())
t.start()
app.mainloop()