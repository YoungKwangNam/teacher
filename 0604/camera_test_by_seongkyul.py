import tkinter as tk
import os, socket
import picamera
import time
import RPi.GPIO as GPIO
SWITCH = 26#GPIO26
RLED = 19
GLED = 13
BLED = 6
GPIO.setmode(GPIO.BCM)
GPIO.setup(SWITCH, GPIO.IN)
GPIO.setup(RLED, GPIO.OUT,initial= GPIO.LOW)
GPIO.setup(GLED, GPIO.OUT,initial= GPIO.LOW)
GPIO.setup(BLED, GPIO.OUT,initial= GPIO.LOW)
r = GPIO.PWM(RLED,100)
g = GPIO.PWM(GLED,100)
b = GPIO.PWM(BLED,100)
rdc = 0.0
gdc = 0.0
bdc = 0.0
r.start(rdc)
g.start(gdc)
b.start(bdc)
root = tk.Tk()
root.title("Photo upload")
root.geometry("630x300+100+100")
foldername = tk.StringVar()
filename = tk.StringVar()
effectnum = tk.IntVar()
effect = ['negative','colorswap','film','blur','pastel','sketch','watercolor']
def Exit():
    global root
    #print(foldername.get())
    root.destroy()
    GPIO.remove_event_detect(SWITCH)
    GPIO.cleanup()
def ShotBySW(p):
    Shot()
def Shot():
    global root
    global image
    global imageScreen
    global filename
    global foldername
    mk_dir()
    # take photo and save
    camera = picamera.PiCamera()
    camera.resolution = (320,240)
    camera.image_effect = effect[effectnum.get()]
    r.ChangeDutyCycle(100.)
    g.ChangeDutyCycle(100.)
    b.ChangeDutyCycle(100.)
    time.sleep(0.05)
    camera.capture(foldername.get()+'/'+'tmp'+".gif")
    camera.capture(foldername.get()+'/'+filename.get()+".jpg")
    camera.close()
    time.sleep(0.05)
    r.ChangeDutyCycle(0.)
    g.ChangeDutyCycle(0.)
    b.ChangeDutyCycle(0.)
    # show on the screean
    image = tk.PhotoImage(file = foldername.get()+'/'+'tmp'+".gif")
    imageScreen = tk.Label(root, image = image)
    imageScreen.place(   x=300 , y=10 )
    print(effect[effectnum.get()])
def up(soc):
    f_dir = foldername.get()
    f_name = filename.get()+".jpg"
    f_size = os.path.getsize(f_dir+'/'+f_name)
    f = open(f_dir+'/'+f_name, 'rb')
    body = f.read()
    f.close()
    soc.sendall((f_name+'/'+str(f_size)).encode())
    time.sleep(0.5)
    soc.sendall(body)
def Upload():
    HOST = '192.168.22.127'#'192.168.22.127'#'192.168.103.61'
    PORT = 9999
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect((HOST, PORT))
    client_socket.sendall('up'.encode())
    up(client_socket)
    time.sleep(0.5)
    client_socket.sendall('stop'.encode())
    client_socket.close()
def mk_dir():
    global foldername
    if not os.path.isdir(foldername.get()):
        print(foldername.get() + '디렉토리 생성')
        os.mkdir(foldername.get())
GPIO.add_event_detect(SWITCH,GPIO.RISING,ShotBySW,300)
folderLabel = tk.Label(root, text="folder name")
folderTextBox = tk.Entry(root, width=20, textvariable=foldername)
fileLabel = tk.Label(root, text="file name")
fileTextBox = tk.Entry(root, width=20, textvariable=filename)
shotButton = tk.Button(root, text="Shot!", command = Shot)
effectRadioButton1 = tk.Radiobutton(root,text = "negative"  , value=0, variable = effectnum)
effectRadioButton2 = tk.Radiobutton(root,text = "colorswap" , value=1, variable = effectnum)
effectRadioButton3 = tk.Radiobutton(root,text = "film"      , value=2, variable = effectnum)
effectRadioButton4 = tk.Radiobutton(root,text = "blur"      , value=3, variable = effectnum)
effectRadioButton5 = tk.Radiobutton(root,text = "pastel"    , value=4, variable = effectnum)
effectRadioButton6 = tk.Radiobutton(root,text = "sketch"    , value=5, variable = effectnum)
effectRadioButton7 = tk.Radiobutton(root,text = "watercolor"    , value=6, variable = effectnum)
#image = tk.PhotoImage(file = "sung.gif")
#imageScreen = tk.Label(root, image = image)
uploadButton = tk.Button(root, text="Upload", command = Upload)
exitButton = tk.Button(root, text="Exit", command = Exit)
folderLabel.place(   x=20  , y=10 )
folderTextBox.place( x=120 , y=10 )
fileLabel.place(     x=20  , y=40 )
fileTextBox.place(   x=120 , y=40 )
shotButton.place(    x=125 , y=100 )
effectRadioButton1.place(  x=60 , y=160)
effectRadioButton2.place(  x=60 , y=190)
effectRadioButton3.place(  x=60 , y=220)
effectRadioButton7.place(  x=60 , y=250)
effectRadioButton4.place(  x=175 , y=160)
effectRadioButton5.place(  x=175 , y=190)
effectRadioButton6.place(  x=175 , y=220)
#imageScreen.place(   x=300 , y=10 )
uploadButton.place(  x=470 , y=260 )
exitButton.place(    x=570 , y=260 )
root.mainloop()