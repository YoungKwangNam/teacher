import tkinter as tk

class Application(tk.Frame):
    def __init__(self,master=None):
        super().__init__(master)
        self.master=master
        self.master.geometry('1600x900+100+100')
        self.master.resizable(False,False)
        self.pack()
        self.create_widgets()
        
        
    def create_widgets(self):
        self.dir_l= tk.Label(self,width = 60, font=60, text='Folder dir:')
        self.dir_l.grid(row=0,column=0)
        self.dir_e=tk.Entry(self,width=60).grid(row=0,column=1)
        
        
        self.dir_l= tk.Label(self, width = 60, font=60, text='File name:')
        self.dir_l.grid(row=1,column=0)
        self.dir_e=tk.Entry(self,width=60).grid(row=1,column=1)
        
        self.save_btn=tk.Button(self,width=60,font=60, text='CAPTURE').grid(row=2,column=0)
        
        self.effect_l=tk.Label(self,width = 60, font=60, text='Picture effect').grid(row=3,column=0)
        
        effect=[('none',1),('oilpaint',2),('negative',3)]
        self.eff= tk.IntVal()


root = tk.Tk()
app = Application(master=root)

cnt = 0
imgs = None