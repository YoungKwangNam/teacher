def myRead(f_name):
    f = open(f_name, 'r', encoding='utf-8')
    print(f.read())
    f.close()

def myWrite(f_name):
    f = open(f_name, 'w', encoding='utf-8')
    while True:
        s = input('msg(/stop):')
        if s == '/stop':
            break
        else:
            f.write(s+'\n')
    f.close()

def my_appenWrite(f_name):
    f = open(f_name, 'a', encoding='utf-8')
    while True:
        s = input('msg(/stop):')
        if s == '/stop':
            break
        else:
            f.write(s+'\n')
    f.close()

def main():
    f_name = 'b.txt'
    print(f_name,'의 원래 내용')
    myRead(f_name)
    
    my_appenWrite(f_name)
    print(f_name,'의 이어쓰기 후 내용')
    myRead(f_name)
    
    myWrite(f_name)
    print(f_name,'의 쓰기 후 내용')
    myRead(f_name)
    
main()
