def nums():
    l1 = []#빈 리스트 생성
    while True:
        n = int(input('숫자를 입력하라, 멈추려면 0'))
        if n==0:
            break
        l1.append(n)
    return l1

def printNums(l1):
    for i in l1:
        print(i, end=',')
    print()

def main():
    l1 = nums()
    printNums(l1)

main()
