onevent = None

def onHandler(f):
    global onevent
    onevent = f

def handler():
    print('짝', end='')

def parsingNum(num):
    s = str(num)
    flag = False
    for i in s:
        n = int(i)
        if n!=0 and n%3==0:
            onevent()
            flag = True
    if flag:
        print()
    else:
        print(num)

def main():
    onHandler(handler)
    for i in range(1, 101):
        parsingNum(i)

main()
