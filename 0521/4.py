def fact(num):
    if num==1:
        return 1
    else:
        return num*fact(num-1)

def fact2(num):
    res=1
    while num>1:
        res*=num
        num-=1
    return res
        

def fibo(n):
    if n<=2:
        return 1
    else:
        return fibo(n-2)+fibo(n-1)
    
def main():
    for i in range(1, 101):
        num = fibo(i)
        print(num, end=',')
        
    '''
    res = fact(4)
    print(res)
    res = fact2(4)
    print(res)
    '''

main()
