def main():
    str1 = "abcdefghijklmnopqrstuvxyz"
    f = open("f.txt", "w")
    f.write(str1)
    f.truncate(10)
    f.close()

    f = open("f.txt", "r")   
    print(f.read())
    f.close()

main()
