def main():
    str1 = "abcdefghijklmnopqrstuvxyz"
    f = open("e.txt", "w")
    f.write(str1)
    f.close()

    #seek() 함수로 위치를 제어할때 상대위치를 사용하려면 바이너리 모드이어야 함
    f = open("e.txt", "rb")    
    print("현재 위치 :", f.tell())
    print(f.read(1))

    f.seek(5)  #맨 앞에서 5칸 뒤로 이동
    print("현재 위치 :", f.tell())
    print(f.read(1))
    
    f.seek(10, 0)  #맨 앞에서 10칸 뒤로 이동
    print("현재 위치 :", f.tell())
    print(f.read(1))
    
    f.seek(-5, 1)   #현재 위치에서 앞으로 5칸 이동
    print("현재 위치 :", f.tell())
    print(f.read(5))
    
    f.seek(5, 1)    #현재 위치에서 뒤로 5칸 이동
    print("현재 위치 :", f.tell())
    print(f.read(3))
    
    f.seek(-3, 2)   #끝 위치에서 앞으로 3칸 이동
    print("현재 위치 :", f.tell())
    print(f.read())
    f.close()

main()
