import os

def main():
    old_name = input("이름을 수정할 파일명을 입력하시오:  ")
    new_name = input("새 파일명을 입력하시오:  ")

    os.rename(old_name, new_name)

    f = open(new_name, "r")
    print(f.read())
    f.close()

main()
