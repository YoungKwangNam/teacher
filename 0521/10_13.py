import os

def main():
    f = open("rem.txt", "w+")
    f.write("hello Python")
    f.seek(0)
    print(f.read())
    f.close()

    os.remove("rem.txt")

    f = open("rem.txt", "r")
    print(f.read())
    f.close()

main()
