def write_strs(file_name, strs):
    with open(file_name, "w") as f:
        f.writelines(strs)

def read_strs(file_name):
    with open(file_name, "r") as f:
        str1 = f.read()

    return str1

def read_strs2(file_name):
    print("함수 read_strs2() 호출")
    with open(file_name, "r") as f:
        for line in f:
            print(line)

def main():
    list1 = ["aaa\n", "bbb\n", "ccc\n"]
    f_name = "d.txt"
    write_strs(f_name, list1)
    str1 = read_strs(f_name)
    print(f_name, "내용 :", str1)
    read_strs2(f_name)
    
main()
    
