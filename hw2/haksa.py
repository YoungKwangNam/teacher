class OpenInfo:
    def __init__(self, room, day, time, credit, number, fname):
        self.room = room
        self.day = day
        self.time = time
        self.credit = credit
        self.number = number
        self.numcnt = 0
        self.infofile = fname

    def editNumcnt(self, num):
        if num>0 and self.numcnt >= self.number:
            print('수강인원 찼다, 수강신청 취소됨')
        else:
            self.numcnt += num

    def printOpenInfo(self):
        print('room:', self.room,'/day:',self.day,'/time:',self.time,'/학점:',
              self.credit,'/인원수:', self.number, '/신청인원:', self.numcnt)
        
        
class Subject:
    def __init__(self, code=0, name='', openflag=False):
        self.code = code
        self.name = name
        self.openflag = openflag
        self.openInfo = None

    def printSubject(self):
        print('code:', self.code, '/name:', self.name,'/개설여부:',
              self.openflag)
        if self.openflag:
            self.openInfo.printOpenInfo()

class Dao:
    def __init__(self):
        self.subs = []

    def insert(self, sub):
        self.subs.append(sub)

    def select(self, code):
        for sub in self.subs:
            if code == sub.code:
                return sub

    def selectByOpenFlag(self, flag):
        res = []
        for sub in self.subs:
            if sub.openflag == flag:
                res.append(sub)
        return res

    def printAll(self):
        for sub in self.subs:
            sub.printSubject()

        
class StaffService:
    def __init__(self, dao):
        self.dao = dao

    def addSubject(self):
        code = input('code:')
        name = input('name:')

        self.dao.insert(Subject(code, name))

    def printSubjects(self):
        self.dao.printAll()

#전역함수. 과목코드가 리스트에 포함되었나 확인해줌
def check(code, l1):
    for sub in l1:
        if sub.code == code:
            return True
    return False

class ProfService:
    def __init__(self, dao):
        self.dao = dao

    def openSub(self):
        subs = self.dao.selectByOpenFlag(False)
        for sub in subs:
            sub.printSubject()

        flag = False
        while not flag:
            code = input('개설할 과목 코드:')
            flag = check(code, subs)

        sub = self.dao.select(code)
            
        room = input('강의실:')
        day = input('요일:')
        time = input('시간:')
        credit = input('학점:')
        number = int(input('수강인원'))
        fname = code+'.txt'
        info = input('강의에 대한 설명을 작성하시오')
        f = open(fname, 'w')
        f.write(info)
        f.close()
        sub.openflag = True
        sub.openInfo = OpenInfo(room, day, time, credit, number, fname)
        print(sub.name, ' 과목이 개설되었습니다.')

    def printOpenSub(self):
        subs = self.dao.selectByOpenFlag(True)
        for sub in subs:
            sub.printSubject()
           
    def cancelOpenSub(self):
        subs = self.dao.selectByOpenFlag(True)
        for sub in subs:
            sub.printSubject()
        
        flag = False
        while not flag:
            code = input('개설 취소할 과목 코드:')
            flag = check(code, subs)
        
        sub = self.dao.select(code)
        sub.openflag = False
        sub.openInfo = None

class Menu:
    def __init__(self):
        self.dao = Dao()
        self.sta = StaffService(self.dao)
        self.prof = ProfService(self.dao)

    def run(self):
        print('menu')
        while True:
            m = input('1.교직원 2.교수3.학생 4.종료')
            if m=='1':
                self.run_s()
            elif m=='2':
                self.run_p()
            elif m=='3':
                print('학생기능')
            elif m=='4':
                break

    def run_s(self):
        while True:
            m = input('1.과목등록 2.과목목록 3.종료')
            if m=='1':
                self.sta.addSubject()
            elif m=='2':
                self.sta.printSubjects()
            elif m=='3':
                break
        
    def run_p(self):
        while True:
            m = input('1.과목개설 2.개설목록 3.개설취소 4.종료')
            if m=='1':
                self.prof.openSub()
            elif m=='2':
                self.prof.printOpenSub()
            elif m=='3':
                self.prof.cancelOpenSub()
            elif m=='4':
                break

def main():
    m = Menu()
    m.run()

main()
