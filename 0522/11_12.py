def printNum():
    try:
        num = int(input("1~10 사이의 정수를 입력하라:"))

    except ValueError as e:
        print("valueError 발생:", e)

    else:
        if num < 1 or num > 10 :
            raise RuntimeError("num은 1~10 사이어야 함")

        for i in range(1, num+1):
            print(i)

def main():
    try:
        printNum()
        printNum()
        printNum()

    except RuntimeError as e:
        print(e)

    except:
        print("예상치 못한 예외 발생")

main()
