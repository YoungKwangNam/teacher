class MyError(Exception):
    
    def __init__(self, msg1, msg2): #생성자. 모든 메서드의 첫번째 파라메터는 this이다.
        self.msg1 = msg1      #self.msg:멤버변수
        self.msg2 = msg2
        #a = 'asdf'          #a:지역변수

def main():
    try:
        raise MyError("occur my error", 'bbb')
        print("예외 발생으로 이 문장은 실행안됨")
    except MyError as e:
        print(e)
        
    print("프로그램은 중단되지 않는다")
    
main()
