def f1():
    print("f1() 시작")
    f2()
    print("f1() 종료")

def f2():
    print("f2() 시작")
    f3()
    print("f2() 종료")

def f3():
    print("f3() 시작")
    raise RuntimeError("f3()에서 발생한 RuntimeError")
    print("f3() 종료")

def main():
    print("main() 시작")
    try:      
        f1()
    except RuntimeError as e:
        print("main에서 ", e, " 예외처리")

    print("main() 종료")

main()
