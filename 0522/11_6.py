def main():
    list1 = [1,2,3]
    try:
        #print(list1[5])
        #result = 3/0
        print("result=", result)
        
    except (ZeroDivisionError, IndexError):
        print("0으로 나누거나 인덱스가 잘못되었음.")
    except:
        print('예기치 않은 예외 발생')
    else:
        print("예외가 발생하지 않았다.")
    finally:
        print('예외 발생 여부와 상관없이 무조건 실행')

    print("프로그램은 중단되지 않는다")

main()
