import os

def readDir(path):
    os.chdir(path)#파라메터로 받은 디렉토리로 이동
    print(os.getcwd(), '디렉토리로 이동됨')#현재 디렉토리 출력
    files = os.listdir('./')#현재 디렉토리의 파일 목록을 읽어옴
    print('파일목록')
    for f in files:#파일 수 만큼 루프
        if os.path.isfile(f):
            print(f)
        else:
            readDir(f)
    os.chdir('../')

def main():
    d = input('디렉토리 경로를 작성하시오')
    if os.path.isdir(d):
        readDir(d)
    else:
        print('잘못된 디렉토리 경로')

main()
