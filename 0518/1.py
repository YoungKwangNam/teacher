m = input('긴 메시지를 입력하시오. 영어로')
msg = m.lower()     #lower():소문자로 변환
al = [0]*26     #리스트를 0으로 초기화해서 26칸 만듬.
s = ord('a')    #ord(문자):아스키코드값 반환
for i in msg:
    for j in range(0, 26):
        if i == chr(j+s):   #chr(아스키코드):그 코드의 문자 반환
            al[j]+=1

for j in range(0, 26):
    print(chr(j+s),':',al[j], end=' / ')
    if j%5==4:
        print()
    
