list1 = list(range(1, 6))

for i in range(0, len(list1)):  #i: 리스트 인덱스 
    print(list1[i], end=',')
print()

for i in list1:                 #i: 리스트 요소
    print(i, end=', ')
print()


list2 = ['aaa', 'bbb', 'ccc']
for idx, i in enumerate(list2):
    print('list2[', idx,']=', i)

list3 = list(range(1,11))
a = list3[3:7]
print(a)
b = list3[3:7:2]
print(b)


print(list3)
list3[2] = 30

print(list3, '/ len=', len(list3))

del list3[2]
print(list3, '/ len=', len(list3))

del list3
#print(list3)

list4 = list(range(1,11))
list4.remove(4)
print(list4)
#list4.remove(4)
#print(list4)
list4.clear()
print(list4)
del list4
print(list4)
