class Parent:
    def __init__(self, x=1, y=2):
        self.x = x
        self.y = y
        self.__k = 100

    def printxy(self):
        print('(',self.x,':',self.y,')')
        print(self.__k)

class Child(Parent):
    def __init__(self, x, y, z):
        #super().__init__()
        super().__init__(x, y)
        self.z = z

    def printxy(self):
        print('(',self.x,':',self.y,':',self.z,')')
        

def main():
    p = Parent(1,2)
    p.printxy()

    c = Child(10, 20, 30)
    c.printxy()

main()
