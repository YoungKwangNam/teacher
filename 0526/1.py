class Parent:
    def __init__(self):
        print('부모 생성자')
        self.name='parent'
        self.age=34

    def sayParent(self):
        print('parent method')

    def printInfo(self):
        print('name:', self.name)
        print('age:', self.age)

class Child(Parent):    #Parent 상속받음
    def __init__(self):
        super().__init__()  #부모 객체의 생성자 호출
        print('자식 생성자')
        self.hobby = '놀기'

    def sayChild(self):
        print('child method')

    def printInfo2(self):
        print('name:', self.name)
        print('age:', self.age)
        print('hobby:', self.hobby)

class Test(Child):
    def __init__(self):
        super().__init__()
        print('Test 생성자')

    def test(self):
        print('test')

def main():
    p = Parent()
    p.sayParent()
    p.printInfo()

    c = Child()
    c.sayParent()
    c.sayChild()
    c.printInfo()
    c.printInfo2()

    t = Test()
    t.sayChild()
    t.test()

main()
