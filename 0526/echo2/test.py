#coding: utf-8
import tkinter as tk

root= tk.Tk()
def func(ev):
    label.config(text=e.get())

label=tk.Label(root,text='Input Text')
label.pack()
e=tk.Entry(root)
e.pack()
e.bind('<Return>',func)

def func2():
    label2.config(text='Pushed')
def func2_event(ev):
    label2.config(text='Push Button')
label2 = tk.Label(root,text='Push Button')
label2.pack()
button= tk.Button(root,text= 'Push',command = func2)
button.pack()
button.bind('<Leave>',func2_event)
root.mainloop()