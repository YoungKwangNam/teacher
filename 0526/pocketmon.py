class PocketMon:
    def __init__(self):
        self.hp = 30
        self.exp = 0
        self.level = 1
        self.name = None

    def eat(self):
        print(self.name, '밥먹음')

    def sleep(self):
        print(self.name, '잠잠')

    def exc(self):
        print(self.name, '운동함')

    def play(self):
        print(self.name, '논다')

    def printInfo(self):
        print(self.name, '상태정보')
        print('hp:', self.hp)
        print('exp:', self.exp)
        print('level:', self.level)

    def levelCheck(self):
        print(self.name, '레벨체크')

class Picachu(PocketMon):
    def __init__(self):
        super().__init__()
        print('피카추가 생성되었습니다.')
        self.name = 'picachu'

    def eat(self):
        super().eat() #부모 클래스의 재정의 전 메서드 호출
        self.hp += 3

    def sleep(self):
        super().sleep()
        self.hp += 5

    def exc(self):
        super().exc()
        self.hp -= 5
        self.exp += 5
        if self.hp>0:
            self.levelCheck()
            return True
        else:
            return False

    def play(self):
        super().play()
        self.hp -= 8
        self.exp += 8
        if self.hp>0:
            self.levelCheck()
            return True
        else:
            return False

    def levelCheck(self):
        super().levelCheck()
        if self.exp>=20:
            self.level+=1
            self.exp-=20
            print(self.name, '레벨이 1 증가하였음')

class Gobook(PocketMon):
    def __init__(self):
        super().__init__()
        print('꼬부기가 생성되었습니다.')
        self.name = 'gobook'

    def eat(self):
        super().eat() #부모 클래스의 재정의 전 메서드 호출
        self.hp += 5

    def sleep(self):
        super().sleep()
        self.hp += 8

    def exc(self):
        super().exc()
        self.hp -= 15
        self.exp += 15
        if self.hp>0:
            self.levelCheck()
            return True
        else:
            return False

    def play(self):
        super().play()
        self.hp -= 8
        self.exp += 8
        if self.hp>0:
            self.levelCheck()
            return True
        else:
            return False

    def levelCheck(self):
        super().levelCheck()
        if self.exp>=30:
            self.level+=1
            self.exp-=30
            print(self.name, '레벨이 1 증가하였음')

class Lee(PocketMon):
    def __init__(self):
        super().__init__()
        print('이상해씨가 생성되었습니다.')
        self.name = 'lee-sang-hae'

    def eat(self):
        super().eat() #부모 클래스의 재정의 전 메서드 호출
        self.hp += 10

    def sleep(self):
        super().sleep()
        self.hp += 15

    def exc(self):
        super().exc()
        self.hp -= 15
        self.exp += 15
        if self.hp>0:
            self.levelCheck()
            return True
        else:
            return False

    def play(self):
        super().play()
        self.hp -= 13
        self.exp += 13
        if self.hp>0:
            self.levelCheck()
            return True
        else:
            return False

    def levelCheck(self):
        super().levelCheck()
        if self.exp>=40:
            self.level+=1
            self.exp-=40
            print(self.name, '레벨이 1 증가하였음')

def menu():
    print('캐릭터 선택')
    s = input('1.피카추 2.꼬부기 3.이상해씨')
    if s=='1':
        p = Picachu()
    elif s=='2':
        p = Gobook()
    elif s=='3':
        p = Lee()
    else:
        p = Picachu()
        
    flag = True
    while flag:
        s = input('1.밥먹기 2.잠자기 3.운동하기 4.놀기 5.정보확인 6.종료')
        if s=='1':
            p.eat()
        elif s=='2':
            p.sleep()
        elif s=='3':
            flag = p.exc()
            if not flag:
                print('캐릭터 사망')
        elif s=='4':
            flag = p.play()
            if not flag:
                print('캐릭터 사망')
        elif s=='5':
            p.printInfo()
        elif s=='6':
            flag = False
    print('게임종료')

menu()



        
