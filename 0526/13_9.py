class Aclass:

    def __init__(self):
        print("Aclass 생성자")
        super().__init__() #b클래스 생성자 호출

    def aclass_method(self):
        print("aclass_method")

class Bclass:

    def __init__(self):
        print("Bclass 생성자")
        super().__init__() #c클래스 생성자 호출

    def bclass_method(self):
        print("bclass_method")

class Cclass:

    def __init__(self):
        print("Cclass 생성자")
        super().__init__() #object 생성자 호출

    def cclass_method(self):
        print("cclass_method")

class Dclass(Aclass, Bclass, Cclass):

    def __init__(self):
        print("Dclass 생성자")
        super().__init__()#a클래스 생성자 호출

    def dclass_method(self):
        print("dclass_method")

def main():
    x = Dclass()
    
    x.aclass_method()
    x.bclass_method()
    x.cclass_method()
    x.dclass_method()

main()
