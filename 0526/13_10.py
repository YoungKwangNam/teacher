class Aclass:

    def __init__(self, a, b, c):
        self.__a = a
        super().__init__(b, c)

    def set_a(self, a):
        self.__a = a

    def get_a(self):
        return self.__a

    def aclass_method(self):
        print("aclass_method")

class Bclass:

    def __init__(self, b, c):
        self.__b = b
        super().__init__(c)

    def set_b(self, b):
        self.__b = b

    def get_b(self):
        return self.__b

    def bclass_method(self):
        print("bclass_method")

class Cclass:

    def __init__(self, c):
        self.__c = c
        #super().__init__()

    def set_c(self, c):
        self.__c = c

    def get_c(self):
        return self.__c

    def cclass_method(self):
        print("cclass_method")

class Dclass(Aclass, Bclass, Cclass):

    def __init__(self, a, b, c, d):
        super().__init__(a, b, c)
        self.__d = d

    def set_d(self, d):
        self.__d = d

    def get_d(self):
        return self.__d

    def dclass_method(self):
        print("dclass_method")

def main():
    x = Dclass(1,2,3,4)
    print("a:", x.get_a())
    print("b:", x.get_b())
    print("c:", x.get_c())
    print("d:", x.get_d())

    x.aclass_method()
    x.bclass_method()
    x.cclass_method()
    x.dclass_method()

main()
