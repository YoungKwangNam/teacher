import copy
list1 = [1,2,[3,4,5]]
list2 = copy.deepcopy(list1)

print(list1)
print(list2)

list1[0] = 10
print('list1[0] = 10 실행후')
print(list1)
print(list2)

list1[2][0]=30
print('list1[2][0]=30 실행후')
print(list1)
print(list2)
