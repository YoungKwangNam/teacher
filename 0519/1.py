list1 = [1,2,3,4,5]

#얕은 복사
list2 = list1

print(list1)
print(list2)

list1[1]=20

print(list1)
print(list2)

print('list1 참조값:', id(list1))
print('list2 참조값:', id(list2))

#살짝 깊은 복사: copy.copy()
import copy

list3 = copy.copy(list1)

print(list1)
print(list3)

print('list1 참조값:', id(list1))
print('list3 참조값:', id(list3))

list3[1]=200

print(list1)
print(list3)
