#name, tel, address
#전역변수(함수 밖에서 선언). 모든 함수가 공용으로 사용하는 변수
member = []

def addMember():
    info = []#지역변수(함수안에서 선언). 선언한 함수 안에서만 사용가능.
    info.append(input('name:'))
    info.append(input('tel:'))
    info.append(input('address:'))
    member.append(info)

def printAll():
    for m in member:
        print(m)

def searchByName(name):#찾으면 그 객체를 반환하고 없으면 None을 반환
    for m in member:
        if m[0]==name:
            return m

def printMember():
    name = input('search name:')
    res = searchByName(name)
    if res != None:
        print(res)
    else:
        print('not found')

def editMember():
    name = input('edit name:')
    res = searchByName(name)
    if res != None:
        res[1]=input('new tel:')
        res[2]=input('new address:')
    else:
        print('not found')

def delMember():
    name = input('del name:')
    res = searchByName(name)
    if res != None:
        member.remove(res)
    else:
        print('not found')

def main():
    addMember()
    addMember()
    printAll()
    printMember()
    editMember()
    printAll()
    delMember()
    printAll()

main()
