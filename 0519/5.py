list1 = [3,9,6,2,5,1,8,4]
print(list1)
print('max:', max(list1))
print('min:', min(list1))
print('sum:', sum(list1))
print('len:', len(list1))

list객체 메서드
append(val): 맨끝에 요소 추가
insert(idx, val): idx위치에 요소 추가
extend():확장. 다른 리스트와 결합
remove(val):val을 찾아서 삭제. 없으면 에러
pop():마지막 요소 삭제
pop(idx): idx 위치 요소 삭제
clear(): 모든 요소 삭제
count(val): val과 동일한 요소 몇개인지 카운트
index(val): val과 동일한 요소의 위치 반환. 없으면 에러
sort():정렬
reverse():요소를 역순으로 뒤집어줌
copy():얕은 복사
deepcopy():깊은 복사
