import RPi.GPIO as GPIO
import tkinter as tk

GPIO.setmode(GPIO.BOARD)

pins=[11,13,15]


for i in pins:
    GPIO.setup(i,GPIO.OUT, initial=GPIO.LOW)  
    
RP = GPIO.PWM(pins[0],100)
GP = GPIO.PWM(pins[1],100)
BP = GPIO.PWM(pins[2],100)

root = tk.Tk()

RL = tk.DoubleVar()
GL = tk.DoubleVar()
BL = tk.DoubleVar()

RL.set(0)
GL.set(0)
BL.set(0)

RP.start(0)
GP.start(0)
BP.start(0)

def Change_dutyR(dc):
    RP.ChangeDutyCycle(RL.get())
def Change_dutyG(dc):
    GP.ChangeDutyCycle(GL.get())
def Change_dutyB(dc):
    BP.ChangeDutyCycle(BL.get())
    
SR = tk.Scale(root, label = 'RLED', orient = 'h',\
              from_=0, to=100, variable = RL,
              command = Change_dutyR)

SG = tk.Scale(root, label = 'GLED', orient = 'h',\
              from_=0, to=100, variable = GL,
              command = Change_dutyG)

SB = tk.Scale(root, label = 'BLED', orient = 'h',\
              from_=0, to=100, variable = BL,
              command = Change_dutyB)
SR.pack()
SG.pack()
SB.pack()

root.mainloop()

RP.stop()
GP.st