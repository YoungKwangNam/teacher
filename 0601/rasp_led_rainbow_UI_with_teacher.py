import RPi.GPIO as GPIO
import time

rainbow = [{'red':100, 'green':0, 'blue':0},
{'red':100, 'green':50, 'blue':0},
{'red':100, 'green':100, 'blue':0},
{'red':0, 'green':50, 'blue':80},
{'red':100, 'green':0, 'blue':100},
{'red':0, 'green':0, 'blue':100},
{'red':50, 'green':0, 'blue':80}]

red = None
green = None
blue = None

def mk_color(num):
    r = rainbow[num]['red']
    g = rainbow[num]['green']
    b = rainbow[num]['blue']
    red.ChangeDutyCycle(r)
    green.ChangeDutyCycle(g)
    blue.ChangeDutyCycle(b)

def main():
    global red
    global green
    global blue
    
    pins=[17, 27, 22]#�ɹ�ȣ 11, 13, 15
    GPIO.setmode(GPIO.BCM)
    for i in pins:
        GPIO.setup(i, GPIO.OUT, initial=GPIO.LOW)
    
    red = GPIO.PWM(pins[0], 100)
    green = GPIO.PWM(pins[1], 100)
    blue = GPIO.PWM(pins[2], 100)

    red.start(0)
    green.start(0)
    blue.start(0)
    cnt = 0
    try:
        while True:
            print(cnt)
            mk_color(cnt)
            time.sleep(1)
            cnt=(cnt+1