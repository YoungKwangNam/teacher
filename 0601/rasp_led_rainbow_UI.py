import tkinter as tk
import RPi.GPIO as GPIO
import time
from functools import partial

pins=[17,27,22] # pin number 11,13,15
GPIO.setmode(GPIO.BCM)
for i in pins:
    GPIO.setup(i, GPIO.OUT,initial = GPIO.LOW)

label= None
root= None
rgb= [0, 0, 0]

red= GPIO.PWM(pins[0],100)
green = GPIO.PWM(pins[1],100)
blue = GPIO.PWM(pins[2],100)
red.start(0)
green.start(0)
blue.start(0)

def to100(num):
    return (num/255.0)*100

def color(n):
    #global red,green,blue,rgb,label
    if n==1:
        rgb=[to100(255),0,0]
        label.configure(text='red')
        print('1')
    elif n==2:
        rgb=[to100(100),to100(100),0]
        label.configure(text='orange')
        print('2')
    elif n==3:
        rgb=[to100(255),to100(255),0]
        label.configure(text='yellow')
        print('3')
    elif n==4:
        rgb=[0,to100(255),0]
        label.configure(text='green')
        print('4')
    elif n==5:
        rgb=[0,to100(50),to100(200)]
        label.configure(text='blue')
    elif n==6:
        rgb=[0,0,to100(25