import RPi.GPIO as GPIO
import time


pins=[17,27,22] # pin number 11,13,15
GPIO.setmode(GPIO.BCM)
for i in pins:
    GPIO.setup(i, GPIO.OUT,initial = GPIO.LOW)

red= GPIO.PWM(pins[0],100)
green = GPIO.PWM(pins[1],100)
blue = GPIO.PWM(pins[2],100)

red.start(0)
green.start(0)
blue.start(0)

try:
    while True:
        print('red')
        red.ChangeDutyCycle(100)
        green.ChangeDutyCycle(0)
        blue.ChangeDutyCycle(0)
        time.sleep(1)
        print('green')
        red.ChangeDutyCycle(0)
        green.ChangeDutyCycle(100)
        blue.ChangeDutyCycle(0)
        time.sleep(1)
        print('blue')
        red.ChangeDutyCycle(0)
        green.ChangeDutyCycle(0)
        blue.ChangeDutyCycle(100)
        time.sleep(1)

except KeyboardInterrupt:
    pass

red.stop()
green.stop()
blue.stop()

GPIO.cleanup()