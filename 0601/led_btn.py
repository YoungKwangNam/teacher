import RPi.GPIO as GPIO
import time
import tkinter as tk
from functools import partial  

rainbow = [{'red':100, 'green':0, 'blue':0},
{'red':100, 'green':50, 'blue':0},
{'red':100, 'green':100, 'blue':0},
{'red':0, 'green':50, 'blue':80},
{'red':100, 'green':0, 'blue':100},
{'red':0, 'green':0, 'blue':100},
{'red':50, 'green':0, 'blue':80}]

red = None
green = None
blue = None
root = None

def mk_color(num):
    print(num)
    r = rainbow[num]['red']
    g = rainbow[num]['green']
    b = rainbow[num]['blue']
    red.ChangeDutyCycle(r)
    green.ChangeDutyCycle(g)
    blue.ChangeDutyCycle(b)

def ui_init():
    global root
    
    root = tk.Tk() #윈도우 창

    root.title('chatting room')#윈도우 제목창 타이틀 출력
    root.geometry("200x400")#윈도우 가로 세로 길이
    frm = tk.Frame(root)    #윈도우에 프레임을 생성

    #버튼 생성
    txt = ["red","orange","yellow","green","blue","navy","purple"]    
    b=[]
    for i in range(0, 7):
        b.append(tk.Button(root, text=txt[i], 
        command = partial(mk_color, i)))

    #위젯들을 프레임에 부착
    frm.pack()
    for btn in b:
        btn.pack()
    
def main():
    global red
    global green
    global blue
    global root

    pins=[17, 27, 22]#핀번호 11, 13, 15
    GPIO.setmode(GPIO.BCM)
    for i in pins:
        GPIO.setup(i, GPIO.OUT, initial=GPIO.LOW)

    red = GPIO.PWM(pins[0], 100)
    green = GPIO.PWM(pins[1], 100)
    blue = GPIO.PWM(pins[2], 100)

    red.start(0)
    green.start(0)
    blue.start(0)
    
    ui_init()
    root.mainloop()

    red.stop()
    green.stop()
    blue.stop()

    GPIO.cleanup()

main()

