import cv2

img1 = cv2.imread('1.jpg')
img2 = cv2.imread('2.jpg')

img3 = cv2.bitwise_and(img1, img2)
img4 = cv2.bitwise_or(img1, img2)
img5 = cv2.bitwise_not(img2)
img6 = cv2.bitwise_xor(img1, img2)
'''
imgh = cv2.hconcat(img1, img2)
cv2.hconcat(img1, img3)
cv2.hconcat(img4, img5)
cv2.hconcat(img4, img6)

cv2.vconcat(img1, img4)
'''
cv2.imshow('img', img6)
cv2.waitKey(0)
cv2.destroyAllWindows()
