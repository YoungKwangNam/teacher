#-*- coding:utf-8 -*-
import cv2
from matplotlib import pyplot as plt # as는 alias 적용시 사용

img = cv2.imread('./img/earth.jpg', cv2.IMREAD_COLOR)
ww=640
hh=320
img2=cv2.resize(img, (ww, hh))

''''
b, g, r = cv2.split(img)   # img파일을 b,g,r로 분리
img2 = cv2.merge([r,g,b]) # b, r을 바꿔서 Merge

plt.imshow(img2)
x=plt.xticks([]) # x축 눈금
y=plt.yticks([]) # y축 눈금
plt.show()
'''
#cv2.waitKey(0)
#cv2.destroyAllWindows()

y00=hh/2-20
y01=y00+50
x00=ww/6-20
x01=x00+40
h=y01-y00
w=x01-x00
moon = img2[int(y00):int(y01),int(x00):int(x01)] # img[행의 시작점: 행의 끝점, 열의 시작점: 열의 끝점]

y10=y00+100
y11=y10+h
x10=x00+100
x11=x10+w

b, g, r = cv2.split(img2)   # img파일을 b,g,r로 분리
img3 = cv2.merge([r,g,b]) # b, r을 바꿔서 Merge
img3[int(y10):int(y11),int(x10):int(x11)] = moon # 동일 영역에 Copy
cv2.imshow('img',img3)
cv2.waitKey(0)
cv2.destroyAllWindows()