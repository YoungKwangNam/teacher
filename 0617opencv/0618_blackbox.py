########블랙박스 만들기#########################
#영상 촬영(1분 단위-날짜시간.avi)
#촬영모드-(업그레이드)얼굴이 나오는 영상은 별도 경로에 저장
#보기-클릭-파일목록(라디오버튼)+보기버튼 -> 영상 뜸
#(업그레이드)-얼굴 안나오는 영상과 얼굴 나오는 영상을 구별해서 목록(라디오 버튼)+보기버튼(공통)-> 영상 재생
###############################################

import cv2
import tkinter as tk
import threading
import os
global window2
import time
from datetime import datetime
import shutil

fps=25



#CascadeClassifier xml 파일의 경로 #haarcascade_frontalface_alt2.xml
cascade_file = 'C:\\Users\\Playdata\\Downloads\\opencv-master\\opencv-master\\data\\haarcascades\\haarcascade_frontalface_default.xml'
cascade = cv2.CascadeClassifier(cascade_file)




class Application(tk.Frame):

   def __init__(self, master=None):
       super().__init__(master)
       self.master=master
       self.master.geometry('200x250+300+200')
       self.create_widgets()

   def create_widgets(self):
       self.button1= tk.Button(self.master, font= 60, text='record event',command=self.button1_command)
       self.button1.place(x=50, y=75)

       self.button2= tk.Button(self.master, font=60, text='show list',command = self.button2_command)
       self.button2.place(x=60, y=150)
   #################################################33
   def button1_command(self):
       #녹화하는 동안 while문이 돌기 때문에 쓰레드
       tv_recording=threading.Thread(target=self.video_record,args=())
       tv_recording.start()


   '''
   def find_avi_list(self):#디렉토리에 avi파일이 몇개 있는지 카운트
       dir_path = os.getcwd()
       file_list = os.listdir(dir_path + "/avi files")
       file_list.sort()
       file_list_avi = []
       for item in file_list:
           # .avi가 파일 이름에 없을 경우 -1을 반환
           if item.find('.avi') is not -1:
               file_list_avi.append(item)
       return(len(file_list_avi))
   '''
   def video_record(self):#비디오 녹화 버튼 명령어
       global fps
       global cascade
       cap = cv2.VideoCapture(0)
       fourcc = cv2.VideoWriter_fourcc(*'DIVX')

       #cnt=self.find_avi_list()#디렉토리에 있는 avi파일의 개수
       #out = cv2.VideoWriter("./avi files/video"+str(cnt+1)+".avi", fourcc, 25.0, (640, 480))#cnt+1번째 avi파일 생성

       now=datetime.now()#현재 시간으로 파일 이름 지정
       save_name=str(now.year)+str(now.month)+str(now.day)+"_"+str(now.hour)+str(now.minute)+str(now.second)+".avi"
       dir_name="./avi files/event/"
       seconds=3#촬영 시간
       shots=seconds*fps#shot횟수
       out = cv2.VideoWriter(dir_name+save_name, fourcc, fps, (640,480))

       face_in_video=False#비디오 파일 속에 얼굴 있는지
       while (cap.isOpened()):
           for i in range(0,shots):#정해진 초만큼 녹화하기 위함.

               ##################영상에 넣을 글자 변수
               fps_str = "FPS : %0.1f" % fps
               seconds_str = "seconds : "+ str(int(i/fps)) +"/"+str(seconds)
               msg_str="Face detected!"
               ##################영상 녹화
               ret, frame = cap.read()
               face_now=False#실시간 영상 속에 얼굴 인식
               if ret:
                   # 이미지 반전,  0:상하, 1 : 좌우
                   frame = cv2.flip(frame, 1)
                   #얼굴 인식
                   gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                   face_list = cascade.detectMultiScale(gray, scaleFactor=1.1, minNeighbors=1, minSize=(170, 170))

                   if len(face_list) > 0:
                       print(face_list)
                       color = (0, 0, 255)
                       for face in face_list:
                           x, y, w, h = face
                           cv2.rectangle(frame, (x, y), (x + w, y + h), color, thickness=8)
                           face_now=True

                   else:
                       #print("no face")
                        pass


                   cv2.putText(frame, fps_str, (25, 20), cv2.FONT_HERSHEY_PLAIN, 1, (255, 255, 255))
                   cv2.putText(frame, seconds_str, (25, 30), cv2.FONT_HERSHEY_PLAIN, 1, (255, 255, 255))
                   if face_now==True:
                        cv2.putText(frame, msg_str, (25, 40), cv2.FONT_HERSHEY_PLAIN, 1, (255, 255, 255))
                        face_in_video=True

                   #영상 화면 띄우기
                   cv2.imshow('frame', cv2.resize(frame, (int(640*1.5), int(480*1.5))))
                   #영상을 파일에 쓰기
                   out.write(frame)
                   if cv2.waitKey(1) & 0xFF == ord('q'):#추가 종료 조건. q 누르기
                       break


           else:#정해진 시간 지나고 종료
               break
       time.sleep(0.5)
       cap.release()
       out.release()
       cv2.destroyAllWindows()

       if face_in_video==True:#얼굴 찍힌 영상은 따로 폴더에 옮기기.
           filename=save_name
           src=dir_name
           dir=dir_name+"face_event/"
           shutil.move(src+filename,dir+filename)

   def button2_command(self):
       window2(self)

#######################################################

class window2(Application):
   def __init__(self, master):
       self.master=master
       self.newWindow = tk.Toplevel(self.master)
       self.newWindow.geometry('200x400+500+200')
       self.create_widgets2()


   def create_widgets2(self):
       # labelExample = tk.Label(newWindow, text="New Window")
       # buttonExample = tk.Button(newWindow, text="New Window button")
       # radiobuttonExample = tk.Radiobutton(newWindow, text = "New Window radiobutton")

       # labelExample.pack()
       # buttonExample.pack()
       # radiobuttonExample.pack()

       # 디렉토리 내의 파일 리스트

       ###############Undetected################
       labelExample = tk.Label(self.newWindow, text="Face undetected:")
       labelExample.pack()

       dir_path = os.getcwd()
       file_list = os.listdir(dir_path + "/avi files/event")
       file_list.sort()
       self.file_list_avi = []
       for item in file_list:
           # .avi가 파일 이름에 없을 경우 -1을 반환
           if item.find('.avi') is not -1:
               self.file_list_avi.append(item)

       self.ra = []
       self.radioval = tk.IntVar()
       self.idx_normal =0
       for idx, fn in enumerate(self.file_list_avi):
           self.ra.append(tk.Radiobutton(self.newWindow, text=fn, variable=self.radioval, value=idx,
                                         command=self.radiobutton_command))
           self.ra[len(self.ra) - 1].pack()
           self.idx_normal=idx


       ###############Detected################
       labelExample = tk.Label(self.newWindow, text="Face Detected:")
       labelExample.pack()

       dir_path = os.getcwd()
       file_list = os.listdir(dir_path + "/avi files/event/face_event")
       file_list.sort()
       self.file_list_avi_face = []
       for item in file_list:
           # .avi가 파일 이름에 없을 경우 -1을 반환
           if item.find('.avi') is not -1:
               self.file_list_avi_face.append(item)

       self.ra = []
       #self.radioval = tk.IntVar()

       for idx, fn in enumerate(self.file_list_avi_face):
           self.ra.append(tk.Radiobutton(self.newWindow, text=fn, variable=self.radioval, value=idx+self.idx_normal+1,
                                         command=self.radiobutton_command))
           self.ra[len(self.ra) - 1].pack()

       button = tk.Button(self.newWindow, text='show video', command=lambda: self.show_video(self.radioval.get()))#람다 연습
       button.pack()

       button=tk.Button(self.newWindow, text='exit',command=self.close)
       button.pack()
       pass

   def radiobutton_command(self):#라디오 버튼을 눌렀을 때 명령
       #선택된 라디오버튼 고유번호에 해당하는 파일명
       #print(self.file_list_avi[self.radioval.get()])
       pass

   def show_video(self,fnNum):#영상보기 버튼을 눌렀을 때 명령
       dir_path = os.getcwd()  # 현재 디렉토리
       if fnNum<=self.idx_normal:
           print(self.file_list_avi[fnNum])
           fn = self.file_list_avi[fnNum]  # 라디오 버튼이 눌린 파일명
           avi_dir = dir_path + "/avi files/event/" + fn  # 파일 디렉토리
       else:
           fnNum2=fnNum-(self.idx_normal+1)#라디오버튼 값은 3,4,5,6,...이었지만 파일 이름은 리스트에서 1,2,3,4,...
           print(self.file_list_avi_face[fnNum2])
           fn = self.file_list_avi_face[fnNum2]  # 라디오 버튼이 눌린 파일명
           avi_dir = dir_path + "/avi files/event/face_event/" + fn  # 파일 디렉토리

       #새 창 띄워서 while문에 머물기 위해서 쓰레드
       tv=threading.Thread(target=self.camera_app, args=(avi_dir,))
       tv.start()

   def camera_app(self,avi_dir):#File로부터 영상 재생
       global fps
       cap = cv2.VideoCapture(avi_dir)

       try:#쓰지 않으면 resize.cpp:4045: error: (-215:Assertion failed) !ssize.empty() in function 'cv::resize
           #resize 오류 방지 위한 try문.
           while (cap.isOpened()):
               ret, frame = cap.read()

               cv2.imshow('frame',  cv2.resize(frame, (int(640*1.5), int(480*1.5))))
               if cv2.waitKey(int(1/fps*1000)) > 0: break   #프레임 간 딜레이(ms=0.001s)
       except Exception as e:#resize오류에 대한 예외처리입니다.
           print(str(e))

       cap.release()
       cv2.destroyAllWindows()


       '''
       self.cap = cv2.VideoCapture(0)
       print('width: {0}, height: {1}'.format(self.cap.get(3), self.cap.get(4)))
       self.cap.set(3, 320)
       self.cap.set(4, 240)
       while True:
           ret,frame=self.cap.read()# ret : frame capture결과(boolean)    # frame : Capture한 frame
           if (ret):
               cv2.imshow('frame', frame)
               if cv2.waitKey(1) & 0xFF == ord('q'):
                   break
       self.cap.release()
       '''

   def close(self):
       self.newWindow.destroy()

#######################################################

def main():
   root= tk.Tk()
   App1= Application(root)

   root.mainloop()


main()