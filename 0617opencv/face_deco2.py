import cv2

crown = cv2.imread('img/img_deco/crown.png')  # 왕관 이미지
image = cv2.imread('img/img_deco/back.jpg')  # 얼굴이 포함된 사진

# CascadeClassifier xml 파일의 경로
cascade_file = 'C:\\Users\\Playdata\\Downloads\\opencv-master\\opencv-master\\data\\haarcascades\\haarcascade_frontalface_default.xml'

# 이미지 흑백처리
image_gs = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
# openCV 머신러닝 CascadeClassifier 파일 로드, 학습 데이터 로드
cascade = cv2.CascadeClassifier(cascade_file)
# 얼굴 부분 좌표
face_list = cascade.detectMultiScale(image_gs, scaleFactor=1.1, minNeighbors=1, minSize=(170, 170))

if len(face_list) > 0:  # 얼굴이 인식됐을 경우
    y, x, h, w = 0, 0, 0, 0
    for face in face_list:
        y, x, h, w = face  # 반환 순서: 세로좌표, 가로좌표, 세로길이, 가로길이
    row, col, ch = crown.shape  # 왕관의 세로길이, 가로길이, 채널수 반환

    roi = image[0:row, x // 2 + w:x // 2 + w + col]  # 왕관을 갖다붙일 영역
    img2gray = cv2.cvtColor(crown, cv2.COLOR_BGR2GRAY)  # 흑백처리
    ret, mask = cv2.threshold(img2gray, 10, 255, cv2.THRESH_BINARY)  # 배경 추출
    mask_inv = cv2.bitwise_not(mask)  # 왕관 모양 추출

    img_fg = cv2.bitwise_and(crown, crown, mask=mask)  # 왕관 and 연산 ---> 왕관만 따오기
    # cv2.imshow('fg', img_fg)
    img_bg = cv2.bitwise_and(roi, roi, mask=mask_inv)  # 사람 있는 배경 이미지에서 왕관 모양만큼 빼기
    # cv2.imshow('bg', img_bg)

    # 두 이미지 결합
    dst = cv2.add(img_fg, img_bg)
    image[0:row, x // 2 + w:x // 2 + w + col] = dst

    # 결과창 띄우기
    cv2.imwrite("img/img_deco/res.png", image)
    cv2.imshow("result", image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

else:  # 얼굴이 인식되지 않았을 경우
    print("no face")