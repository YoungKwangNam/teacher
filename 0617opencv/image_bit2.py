import cv2

img1 = cv2.imread('./img/4.png')#별 포함 이미지
img2 = cv2.imread('./img/flower.jpg')#꽃 배경

h,w,c= img1.shape #h:세로길이 , w:= 가로길이, c: 채널수
print(h,w,c)

roi= img2[100:100+h,100:100+w]#꽃밭 그림에 별모양을 심을 자리를 추출해서 roi 저장 (100,100)

img1_g=cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)#별 그림의 색상을 흑백으로 변경

#mask 배경 추출용 마스크
ret, mask = cv2.threshold(img1_g, 235, 255, cv2.THRESH_BINARY)

#mask_inv 내용 추출용 마스크
mask_inv= cv2.bitwise_not(mask)

#img_s:별모양만 추출
img_s=cv2.bitwise_and(img1, img1, mask= mask_inv)

#img_b: 꽃밭의 별을 추가할 영역에서 별 모양을 제외한 배경만 추출
img_b= cv2.bitwise_and(roi,roi, mask=mask)

dst=cv2.add(img_s, img_b)

img2[100:100+h, 100:100+w]=dst


cv2.imshow('img',mask_inv)
cv2.imshow('img',img2)
cv2.waitKey(0)
cv2.destroyAllWindows()