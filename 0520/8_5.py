def mutableArg(arg1, arg2):
    print("함수 시작")
    print("변경 전")
    print("arg1:", arg1, ", arg2:", arg2)
    print("arg1의 참조값:", id(arg1), ", arg2의 참조값:", id(arg2))
    arg1 = [100, 200]
    arg2[0] = 400
    arg2[1] = 500
    print("변경 후")
    print("arg1:", arg1, ", arg2:", arg2)
    print("arg1의 참조값:", id(arg1), ", arg2의 참조값:", id(arg2))
    print("함수 종료")

def main():
    list1 = [1,2,3]
    list2 = [4,5]
    print("함수 호출 전")
    print("main의 list1=", list1, ", list2=", list2)
    print("list1의 참조값=", id(list1), ", list2의 참조값=", id(list2))
    mutableArg(list1, list2)
    print("함수 종료 후")
    print("main의 list1=", list1, ", list2=", list2)
    print("list1의 참조값=", id(list1), ", list2의 참조값=", id(list2))

main()
