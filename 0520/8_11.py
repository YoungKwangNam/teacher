a = 10  #전역변수
b = 20  #전역변수

def test1(b):   #파라메터도 지역변수
    print("-----test1()-----")
    a = 100     #지역변수
    c = 300     #지역변수
    print("a =", a, ", b =", b, ", c =", c)
    if True:
        x = 500     #지역변수
        print("if 블록 안 x =", x)
    print("if 블록 밖 x =", x)
    print("------------------")

def test2():
    print("-----test2()-----")
    global a #전역변수
    a = 1234 #전역변수
    b = 4567 #지역변수
    print("a =", a) #전역변수
    print("b =", b) #지역변수
    print("------------------")
    

def test3():
    print("-----test3()-----")
    print("a =", a) #전역변수
    print("b =", b) #전역변수
    print("c =", c) #에러:다른 함수의 지역 변수는 사용할 수 없다
    print("x =", x) #에러:다른 함수의 지역 변수는 사용할 수 없다  
    print("------------------")

def main():
    print("main에서 전역 a =", a)
    print("main에서 전역 b =", b) 
    test1(200)
    test2()
    print("main에서 전역 a =", a)
    print("main에서 전역 b =", b) 
    test3()
    

main()
