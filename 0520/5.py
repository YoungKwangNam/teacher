a=10#전역변수

def f1(arg):
    #a=20#새 지역변수 정의
    global a #전역변수 a를 사용하겠다
    a=100
    print('f1의 a:', a)
    b = 30#지역변수
    print('b:', b)
    print('arg:', arg)

def f2():
    print('f2의 a:', a)#전역변수 a
   # print('b:', b)
    

def main():
    f1(123)
    f2()
    a=500#새 지역변수 정의
    print('main의 a:', a)
    f2()

main()
