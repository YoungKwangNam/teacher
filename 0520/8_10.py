def varArg1(msg1, *msg):
    print("msg의 타입:", type(msg))
    print("고정 인자 개수:", 1)
    print("가변 인자 개수:", len(msg))
    print("msg:", msg1, end=",")
    for m in msg:
        print(m, end=",")
        
    print()
    print("-------------------")

def varArg2(*nums):
    print("nums의 타입:", type(nums))
    print("가변 인자 개수:", len(nums))
    sum = 0
    for n in nums:
        sum += n
    return sum

def main():
    #varArg1()  #에러 발생
    varArg1("kkk")
    varArg1("aaa", "bbb", "ccc");
    varArg1("aaa", "bbb", "ccc", "ddd", "eee");

    print("인자없음 =", varArg2());
    print("-------------------")
    print("1+2+3 =", varArg2(1,2,3));
    print("-------------------")
    print("1+2+3+4+5 =", varArg2(1,2,3,4,5));

main()
