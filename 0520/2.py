def add(a, b):
    return a+b

def sub(a, b):
    return a-b

def mul(a, b):
    return a*b

def dev(a, b):
    if b==0:
        print('0으로 나눌 수 없음')
    else:
        return a/b

def main():
    op = [] #연산자만 추출해서 담을 리스트
    p = []  #연산자 위치 저장
    nums = []   #숫자만 추출해서 정수로 만드어서 담을 리스트
    s = input('수식을 입력하라')
    tmp = list(s)
    for idx, i in enumerate(tmp):
        if not i.isdigit():
            op.append(i)
            p.append(idx)

    start=0    
    for i in range(0, len(p)+1):
        if i==len(p):
            nums.append(int(s[start:]))
        else:
            nums.append(int(s[start:p[i]]))
            start=p[i]+1

    print(nums)
    print(op)
    
    for i in op:
        if i not in '+-*/':
            print('잘못된 연산자 포함. 연산취소')
            return
    
    res=0
    for idx, i in enumerate(op):
        if res == None:
            break
        if idx == 0:
            res = nums[0]
        if i=='+':
            res = add(res, nums[idx+1])
        elif i=='-':
            res = sub(res, nums[idx+1])
        elif i=='*':
            res = mul(res, nums[idx+1])
        elif i=='/':
            res = dev(res, nums[idx+1])

    if res==None:
        print('연산취소')
    else:
        print(s,'=',res)
main()
