def f1(l1):
    print('f1:', l1)
    for i in range(0, len(l1)):
        l1[i] *= 10
    print('f1:', l1)
    print('f1의 l1참조값:', id(l1))

def main():
    l2 = [1,2,3,4,5]
    print('main의 l2참조값:', id(l2))
    print('main. f1 호출전:', l2)
    f1(l2[:])
    print('main. f1 호출뒤:', l2)
    

main()
