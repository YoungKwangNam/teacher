#-*- coding:utf-8 -*-
import cv2
import numpy as np
from cffi.backend_ctypes import xrange
from matplotlib import pyplot as plt
import copy

img = cv2.imread('images/name.jpg')
# [x,y] 좌표점을 4x2의 행렬로 작성
# 좌표점은 좌상->좌하->우상->우하
pts1 = np.float32([[191,470],[115,165],[330,160],[440,415]])
#pts1 = np.float32([[250,410],[190,200],[290,170],[370,390]])

# 좌표의 이동점
pts2 = np.float32([[10,10],[400,10],[400,300],[10,300]])

M = cv2.getPerspectiveTransform(pts1, pts2)
dst = cv2.warpPerspective(img, M, (400,300))

# pts1의 좌표에 표시. perspective 변환 후 이동 점 확인.
cv2.circle(img, (191,470), 10, (255,0,0),-1)
cv2.circle(img, (115,165), 10, (0,255,0),-1)
cv2.circle(img, (330,160), 10, (0,0,255),-1)
cv2.circle(img, (440,415), 10, (0,0,0),-1)

plt.subplot(121),plt.imshow(img),plt.title('image')
plt.subplot(122),plt.imshow(dst),plt.title('Perspective')
plt.show()

img=copy.deepcopy(dst)
canny = cv2.Canny(img,30,70)

laplacian = cv2.Laplacian(img,cv2.CV_8U)
sobelx = cv2.Sobel(img,cv2.CV_8U,1,0,ksize=3)
sobely = cv2.Sobel(img,cv2.CV_8U,0,1,ksize=3)

images = [img,laplacian, sobelx, sobely, canny]
titles = ['Origianl', 'Laplacian', 'Sobel X', 'Sobel Y','Canny']

for i in xrange(5):
    plt.subplot(2,3,i+1),plt.imshow(images[i]),plt.title([titles[i]])
    plt.xticks([]),plt.yticks([])

plt.show()