#-*- coding:utf-8 -*-
import cv2
import numpy as np
from cffi.backend_ctypes import xrange
from matplotlib import pyplot as plt
import random

from PIL import Image
from pytesseract import *
pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract'

def f1(img_name):
    img = cv2.imread('images/'+img_name)
    img = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)




    print(cv2.THRESH_BINARY)
    ret, thresh1 = cv2.threshold(img, 20, 255, cv2.THRESH_BINARY)

    blur = cv2.GaussianBlur(thresh1, (3, 3), 0)
    laplacian = f2(cv2.Laplacian(thresh1,cv2.CV_8U))
    sobelx = f2(cv2.Sobel(thresh1,cv2.CV_8U,1,0,ksize=3))
    sobely = f2(cv2.Sobel(thresh1,cv2.CV_8U,0,1,ksize=3))

    canny = cv2.Canny(thresh1, 20, 60)



    images = [thresh1,blur, laplacian, sobelx, sobely, canny]
    titles = ['Origianl', 'gaussianblur', 'Laplacian', 'Sobel X', 'Sobel Y', 'Canny']
    f3(images,titles)


    for i in xrange(6):
        plt.subplot(3,3,i+1),plt.imshow(images[i]),plt.title([titles[i]])
        plt.xticks([]),plt.yticks([])

    plt.show()

def f2(img):#draw contours
    imgray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(imgray, 125, 255, 0)

    contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    for i in xrange(len(contours)):
        # 각 Contour Line을 구분하기 위해서 Color Random생성
        b = random.randrange(1, 255)
        g = random.randrange(1, 255)
        r = random.randrange(1, 255)

        cnt = contours[i]
        img = cv2.drawContours(img, [cnt], -1, (b, g, r), 2)

    return img

def f3(imgs,titles):

    for img, title in zip(imgs,titles):
        cv2.imwrite('images/' + title+".png",img)

        filename = "images/"+ title+".png"
        image = Image.open(filename)

        text = image_to_string(img, lang="kor")  # 한글: lang = "kor"
        print(title,":",text)


f1("bus_tag2.png")