#-*- coding:utf-8 -*-
import cv2
import numpy as np
from cffi.backend_ctypes import xrange
from matplotlib import pyplot as plt

img = cv2.imread('images/bus.png')
img1 = img.copy()

imgray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
ret, thresh = cv2.threshold(imgray,127,255,0)

contours, hierachy = cv2.findContours(thresh, cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
cnt=contours[4]
'''
i=0
cnt_list=[]
try:
    while True:
        cnt = contours[i] # 1이 손모양 주변의 contour
        cnt_list.append(cnt)
        i+=1
except IndexError:
    print("Error!")
    print(i)

cnt_select=cv2.contourArea(contours[1])
for i in range(1,len(cnt_list)):#제일 넓이가 큰 컨투어 선택
    if cnt_select<cv2.contourArea(contours[i]):
        cnt_select=cv2.contourArea(contours[i])
        cnt=contours[i]
        print(i)
#cnt출력됨
'''
hull = cv2.convexHull(cnt)

cv2.drawContours(img1, [hull], 0,(0,255,0), 3)

titles = ['Original','Convex Hull']
images = [img, img1]

for i in xrange(2):
    plt.subplot(1,2,i+1), plt.title(titles[i]), plt.imshow(images[i])
    plt.xticks([]), plt.yticks([])

plt.show()