#-*- coding:utf-8 -*-
import cv2
import numpy as np
import random
import copy

from cffi.backend_ctypes import xrange
from matplotlib import pyplot as plt

img = cv2.imread('images/imageHierarchy.png')

imgray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
ret, thresh = cv2.threshold(imgray,125,255,0)

img1=copy.deepcopy(img)
img2=copy.deepcopy(img)
img3=copy.deepcopy(img)
img4=copy.deepcopy(img)

contours, hierarchy = cv2.findContours(thresh, cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)
print("RETR_LIST:\n",hierarchy)
for i in xrange(len(contours)):
    #각 Contour Line을 구분하기 위해서 Color Random생성
    b = random.randrange(1,255)
    g = random.randrange(1,255)
    r = random.randrange(1,255)

    cnt = contours[i]
    img1 = cv2.drawContours(img1, [cnt], -1,(b,g,r), 2)

contours, hierarchy = cv2.findContours(thresh, cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
print("RETR_EXTERNAL:\n",hierarchy)
for i in xrange(len(contours)):
    #각 Contour Line을 구분하기 위해서 Color Random생성
    b = random.randrange(1,255)
    g = random.randrange(1,255)
    r = random.randrange(1,255)

    cnt = contours[i]
    img2 = cv2.drawContours(img2, [cnt], -1,(b,g,r), 2)

contours, hierarchy = cv2.findContours(thresh, cv2.RETR_CCOMP,cv2.CHAIN_APPROX_SIMPLE)
print("RETR_CCOMP:\n",hierarchy)
for i in xrange(len(contours)):
    #각 Contour Line을 구분하기 위해서 Color Random생성
    b = random.randrange(1,255)
    g = random.randrange(1,255)
    r = random.randrange(1,255)

    cnt = contours[i]
    img3 = cv2.drawContours(img3, [cnt], -1,(b,g,r), 2)

contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
print("RETR_TREE:\n",hierarchy)
for i in xrange(len(contours)):
    #각 Contour Line을 구분하기 위해서 Color Random생성
    b = random.randrange(1,255)
    g = random.randrange(1,255)
    r = random.randrange(1,255)

    cnt = contours[i]
    img4 = cv2.drawContours(img4, [cnt], -1,(b,g,r), 2)





titles = ['RETR_LIST','RETR_EXTERNAL','RETR_CCOMP','RETR_TREE']
images = [img1,img2,img3,img4]

for i in xrange(4):
    plt.subplot(2,2,i+1), plt.title(titles[i]), plt.imshow(images[i])
    plt.xticks([]), plt.yticks([])

plt.show()

