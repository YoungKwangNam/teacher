#교수-1.개설 2.개설과목목록 3.취소

def openFalsesubs(subs):
    print('개설안된 과목목록')
    of = []
    for sub in subs:
        if sub['openflag'] == False:
            of.append(sub)
    return of

def printopenTrue(subs):
    print('개설과목목록')
    of = []
    for sub in subs:
        if sub['openflag'] == True:
            of.append(sub)
            for key in sub:
                print(key, ':', sub[key])
    return of

def opensubject(subs): 
    of = openFalsesubs(subs)
    print('과목 개설')
    for sub in of:
        for key in sub:
            print(key, ':', sub[key])

    code = input('개설 과목 코드:')
    #요일/시간/강의실/학점/인원수/과목개요
    day = input('강의 요일:')
    time = input('시간(ex. 3-5교시=3,4,5):')
    credit = input('몇학점:')
    number = int(input('인원수:'))
    info = input('과목에 대한 설명을 짤막히 작성:')
    for sub in of:
        if code==sub['code']:
            sub['day']=day
            sub['time']=time
            sub['credit']=credit
            sub['number']=number
            sub['info']=info
            sub['openflag']=True
            sub['numcnt']=0
    

            
def cancelopen(subs):
    printopenTrue(subs)
    print('개설 취소')
    code = input('개설 취소 과목 코드:')
    for sub in subs:
        if code==sub['code']:
            if sub['openflag']==True:
                del sub['day']
                del sub['time']
                del sub['credit']
                del sub['number']
                del sub['info']
                del sub['numcnt']
                sub['openflag']=False
            else:
                print('개설되지 않은 과목')

    
