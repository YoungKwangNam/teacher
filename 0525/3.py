class Test2:
    def __init__(self, x, y):
        self.x = x      #public
        self.__y = y    #private

    def printxy(self):
        print('x:', self.x)
        print('y:', self.__y)

    def sety(self, y):
        self.__y = y

    def gety(self):
        return self.__y

def main():
    t1 = Test2(3,4)
    t1.printxy()
    print('t1.x:', t1.x)
    print('t1.__y:', t1.gety())

main()
