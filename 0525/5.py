val1 = 100  #전역변수

def f1():
    val1 = 200   #f1()의 지역변수
    print('f1 val1:', val1)  #200

    def f2():
        val1 = 300  #f2()의 지역변수
        print('f2 val1:', val1)  #300

    def f3():
        nonlocal val1  #nonlocal: 현재 이 함수의 지역변수 아님. 바로 윗 단계 변수
        print('f3 val1:', val1)  #200

    def f4():
        global val1     #global: 전역변수
        print('f4 val1:', val1)  #100

    f2()
    f3()
    f4()

def main():
    f1()

main()
