class Test:
    '''Test class'''
    PI = 3.14
    def __init__(s, msg):
        s.msg = msg

    def printMem(s):
        print(s.msg)

    def staticMethod():
        print('정적 메서드')
        print('pi:', Test.PI)
        

def main():
    t1 = Test('hello')
    t1.printMem()
    print('t1.__doc__:', t1.__doc__)
    print('t1.__class__:', t1.__class__)
    print('t1.__class__.__name__:', t1.__class__.__name__)
    print('t1.__module__:', t1.__module__)
    print('t1.__class__.__bases__:', t1.__class__.__bases__)
    print('t1.__dict__:', t1.__dict__)

    print('Test.__doc__:', Test.__doc__)
    print('Test.__name__:', Test.__name__)
    print('Test.__module__:', Test.__module__)
    print('Test.__bases__:', Test.__bases__)
    print('Test.__dict__:', Test.__dict__)

main()
