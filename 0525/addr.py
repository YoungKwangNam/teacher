class Member:
    def __init__(self, name='', tel='', address=''):
        self.name = name
        self.tel = tel
        self.address = address

    def printInfo(self):
        print(self.name,' / ', self.tel, ' / ', self.address)

class Dao:
    def __init__(self):
        self.members = []

    def insert(self, m):
        self.members.append(m)

    def select(self, name):
        for m in self.members:
            if name==m.name:
                return m

    def update(self, m):
        m2 = self.select(m.name)
        if m2==None:
            print('not found. update cancel')
        else:
            m2.tel = m.tel
            m2.address = m.address

    def delete(self, name):
        m = self.select(name)
        if m==None:
            print('not found. delete cancel')
        else:
            self.members.remove(m)

class Service:
    def __init__(self):
        self.dao = Dao()

    def addMember(self):
        name = input('name:')
        tel = input('tel:')
        address = input('address:')
        m = Member(name, tel, address)
        self.dao.insert(m)

    def getMember(self):
        name = input('search name:')
        m = self.dao.select(name)
        if m==None:
            print('not found')
        else:
            m.printInfo()

    def editMember(self):
        name = input('edit name:')
        tel = input('new tel:')
        address = input('new address:')
        m = Member(name, tel, address)
        self.dao.update(m)

    def delMember(self):
        name = input('del name:')
        self.dao.delete(name)

    def printAll(self):
        for m in self.dao.members:
            m.printInfo()

class Menu:
    def __init__(self):
        self.service = Service()

    def run(self):
        while True:
            menu = input('1.add 2.search 3.edit 4.delete 5.printAll 6.exit')
            if menu=='1':
                self.service.addMember()
            elif menu=='2':
                self.service.getMember()
            elif menu=='3':
                self.service.editMember()
            elif menu=='4':
                self.service.delMember()
            elif menu=='5':
                self.service.printAll()
            elif menu=='6':
                break

def main():
    menu = Menu()
    menu.run()

main()



        
