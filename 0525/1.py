class Test:
    '''이건 클래스 doc'''
    cnt=0#클래스 변수
    def f1(s, val1, val2):
        s.name = val1
        s.age = val2
        Test.cnt += 1
        s.num = Test.cnt

    def f2(self):
        print('num:', Test.cnt)
        print('name:', self.name)
        print('age:', self.age)

def main():
    t1 = Test()
    t1.f1('aaa', 12)
    t1.f2()
    print('t1의 타입:', type(t1))

    t2 = Test()
    t2.f1('bbb', 23)
    t2.f2()
    print('t2의 타입:', type(t2))
    print(t2.__doc__)

main()
        
