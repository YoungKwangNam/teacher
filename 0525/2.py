class Point:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y
    
    def printxy(self):
        print('x:', self.x)
        print('y:', self.y)

    def __del__(self):
        print(self, '가 소멸됨')

def main():
    p1 = Point(1,2)
    p1.printxy()

    p2 = Point(3,4)
    p2.printxy()

    p3 = Point()
    p3.printxy()

    del p1
    del p2
    del p3
    
main()
