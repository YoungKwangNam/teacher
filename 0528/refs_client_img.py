import os, socket

def dir_list(d):
    return os.listdir(d)

def up(soc):
    f_dir = input('업로드할 파일이 속한 폴더명을 입력하시오m')
    f_list = dir_list(f_dir)
    for idx, f in enumerate(f_list):
        print(idx,'.', f)

    num=-1
    while num<0 or num>=len(f_list):
        num = int(input('업로드할 파일 번호 입력'))
    f_name = f_list[num]
    f_size = os.path.getsize(f_dir+'/'+f_name)
    f = open(f_dir+'/'+f_name, 'rb')
    #f = open('C:/Users/Playdata/Desktop/pythonWork/teacher/dog.jpg', 'rb')
    body = f.read()
    f.close()
    soc.sendall((f_name+'/'+str(f_size)).encode())
    soc.sendall(body)

def main():
    #HOST = '192.168.22.91'
    #HOST = '192.168.137.116'
    #HOST = '192.168.137.40'
    HOST = '192.168.22.125'
    PORT = 9999

    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect((HOST, PORT))

    while True:
        menu = input('1.upload 2.download 3.stop')
        if menu=='1':
            client_socket.sendall('up'.encode())
            up(client_socket)
        elif menu == '2':
            client_socket.sendall('down'.encode())
            print('준비중')
        elif menu == '3':
            client_socket.sendall('stop'.encode())
            break

    client_socket.close()

main()
