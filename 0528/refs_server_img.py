import os, socket

def mk_dir():
    if not os.path.isdir('refs'):
        print('refs 디렉토리 생성')
        os.mkdir('refs')

def dir_list():
    return os.listdir('refs')

def upload(soc):

    data = soc.recv(1024)
    data2= data.decode()
    k=data2.split('/')
    f_name= k[0]
    f_size= int(k[1])
    print('f_name', f_name)
    print('f_size', f_size)
    f_list = dir_list()
    for f in f_list:
        if f_name == f:
            s = f_name.split('.')
            f_name = s[0]+'_1.'+s[1]

    f = open('refs/'+f_name, 'wb')
    data = soc.recv(f_size)
    print('body:', data)
    f.write(data)
    f.close()

def main():
    mk_dir()
    #HOST = '192.168.137.1'  #server ip
    HOST = '192.168.22.91'
    PORT = 9999         #server port

    #server socket open. socket.AF_INET:주소체계(IPV4), socket.SOCK_STREAM:tcp 
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    #포트 여러번 바인드하면 발생하는 에러 방지
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    #바인드:오픈한 소켓에 IP와 PORT 할당
    server_socket.bind((HOST, PORT))

    #이제 accept할 수 있음을 알림
    server_socket.listen()

    client_socket, addr = server_socket.accept()
    print('server start')
    while True:
        data = client_socket.recv(1024)
        menu = data.decode()
        print('menu:', menu)
        if menu=='up':
            upload(client_socket)
        elif menu=='down':
            print('준비중')
        elif menu=='stop':
            break

    client_socket.close()
    server_socket.close()
        
main()








    
